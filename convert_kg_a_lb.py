def convert_kg_a_lb(kg):
    '''(number) -> float
    Convierte en libras los kilogramos correspondientes
    Ejemplos:
    >>> convert_kg_a_lb(100)
    220.46
    '''
    lb = kg * 2.2046
    return lb

def convert_lb_a_kg(lb):
    '''(number) -> float
    Convierte en libras los kilogramos correspondientes
    Ejemplos:
    >>> convert_kg_a_lb(100)
    220.46
    '''
    kg = lb / 2.2046
    return kg
