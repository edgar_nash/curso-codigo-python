def calcular_media(exam_alum):
    '''(lista de listas de [str, number]) -> float
    Devuelve la media de las notas de los examenes de cada alumno
    >>>calcular_media([['E1', 8], ['E2', 7], ['E3', 10]])
    '''
    total = 0
    for item in exam_alum:
        total = total + item[1]
    return total/ len(exam_alum)




>>> alumnos = [['examen 1', 7], ['examen 2', 8], ['examen 3', 9]]
>>> for item in alumnos:
	print(item)

	
['examen 1', 7]
['examen 2', 8]
['examen 3', 9]
>>> alumnos[0][0]
'examen 1'
>>> alumnos[0][1]
7
>>> alumnos[0]
['examen 1', 7]
>>> len(alumnos[1])
2
