def prueba_ambitos():
    def hacer_local():
        algo = 'algo local'
    def hacer_non_local():
        nonlocal algo
        algo = 'algo no local'
    def hacer_global():
        global algo
        algo = 'algo global'
    algo = 'algo de prueba'

    hacer_local()
    print('luego de la asignacion local:', algo)
    hacer_non_local()
    print('luego de la asignacion no local:', algo)
    hacer_global()
    print('luego de la asignacion global:', algo)

prueba_ambitos()
print('In global scope:', algo)