#funcion normal, construye una lista con todos los elementos y reserva
# espacio de memoria para toda la lista
def generaPares(limite):
	num = 1
	miLista = []
	while num < limite:
		miLista.append(num * 2)
		num += 1
	return miLista
print(generaPares(10))

# funcion con generador, mas eficiente solo genera los recursos q necesita,
# entra en estanvai entre la construccion de dos o mas elementos

def generaPares(limite):
	num = 1
	while num < limite:
		yield num * 2
		num += 1
devuelvePares = generaPares(10)
print(next(devuelvePares))
print("Aqui podria ir mas codigo...")
print(next(devuelvePares))
print("Aqui podria ir mas codigo...")
print(next(devuelvePares))
print("Aqui podria ir mas codigo...")
