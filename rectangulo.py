def area_rectangulo (largo, ancho):
    area = largo * ancho
    return area

def perimetro_rectangulo (largo, ancho):
    perimetro = (largo + ancho) * 2
    return perimetro

