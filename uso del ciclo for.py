def contador_vocales(s):
    '''(str) -> int
    Cuenta vocales en un string
    >>> s = "videotutoriales.com"
    >>> contador_vocales(s)
    ('a: 1', 'e: 2', 'i: 2', 'o: 3', 'u: 1')
    >>> s = "viIdeotuUtoriaAleEs.coOm"
    >>> contador_vocales(s)
    ('a: 2', 'e: 3', 'i: 3', 'o: 4', 'u: 2')
    '''
    num_a = 0
    num_e = 0
    num_i = 0
    num_o = 0
    num_u = 0
    for caracter in s:
        if caracter in 'aAá':
           num_a = num_a + 1
        if caracter in 'eEé':
           num_e = num_e + 1
        if caracter in 'iIí':
           num_i = num_i + 1
        if caracter in 'oOó':
            num_o = num_o + 1
        if caracter in 'uUúü':
            num_u = num_u +1
    return "a: "+str(num_a), "e: "+str(num_e), "i: "+str(num_i),"o: "+str(num_o), "u: "+str(num_u)


def cuenta_consonantes(s):
    '''(string) -> (int, int)
    Cuenta consonantes y espacios
    Ejs:
    >>> s = "videotutoriales . com"
    >>> cuenta_consonantes(s)
    ('El numero de consonantes es: 9', 'El # de espacios es: 2')
    '''
    num_cons = 0
    num_espacios = 0
    consonantes = 'bBcCdDfFgGhHjJkKlLmMnNñÑpPqQrRsStTvVwWxXyYzZ'
    for caracter in s:
        if caracter in consonantes:
            num_cons = num_cons + 1
        if caracter in ' ':
            num_espacios = num_espacios + 1
    return "El numero de consonantes es: "+str(num_cons), "El # de espacios es: "+str(num_espacios)

def encuentra_vocales ():
    '''(str) -> (str, int)
    Devuelve la consonante y su posicion hasta que encuentre una vocal
    '''
    cadena = input ("Entra el String: ")
    vocal = 'aAáeEéiIíoOóuUú'
    i = 0
    for caracter in cadena:
        if not cadena[i] in vocal:
            print (cadena[i], i)
            i = i + 1
    if i == len(cadena):
        return "No hay vocales"
    else:
        return cadena[i], i
            

