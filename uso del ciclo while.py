def encuentra_vocal ():
    '''(string) -> (string)
    Muestra consonantes hasta que encuentra una vocal y para
    '''
    cadena = input ("Entra el String:")
    vocal = 'aAáeEéiIíoOóuUú'
    i = 0
    while not cadena[i] in vocal:
        print (cadena[i], i)
        i = i + 1            
        if i == len(cadena):
            return "No hay vocales"
    return cadena[i], i, "Vocal encontrada"
     
def vocal_para ():
    cadena = input("Entra la cadena: ")
    vocal = 'aAáeEéiIíoOóuUú'
    i = 0
    devolver_cadena = ''
    while i < len(cadena) and not cadena[i] in vocal:
        print(cadena[i], i)
        devolver_cadena = devolver_cadena + cadena[i]
        i = i + 1
    if devolver_cadena == '':
        return "Vocal en 1ra posicion"
    else:
        return devolver_cadena
