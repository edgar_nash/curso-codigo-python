def convert_km_a_mi(km):
    '''(number) -> float
    Devuelve el numero de millas correspondientes a los kms pasados
    >>>convert_km_a_mi(32)
    19.88384
    '''
    mi = km * 0.62137
    return mi
