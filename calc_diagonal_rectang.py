import math
def calc_diagonal_rectang ():
    '''(number, number) -> float
    Calcular diagonal de un rectangulo dado sus lados
    Ejs:
    1-. >>> calc_diagonal_rectang()
    Entra el alto10
    Entra el ancho2
    'El valor de la diagonal es: 10.198039027185569'
    2-. >>> calc_diagonal_rectang()
    Entra el alto34.5
    Entra el ancho67.9
    'El valor de la diagonal es: 76.16206404766089'
    '''
    alto = float(input ("Entra el alto"))
    ancho = float(input ("Entra el ancho"))
    diagonal = math.sqrt((alto**2 + ancho**2))
    diagonal = str (diagonal)
    return "El valor de la diagonal es: " + diagonal
