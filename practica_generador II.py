def devuelve_ciudades(*ciudades): # Un * antes del argumento de la funcion siginifica q la funcion recibira un numero indeterminado de argumentos en forma de tupla
	for elemento in ciudades:
		#for subElemento in elemento:
			yield from elemento


ciudades_devueltas = devuelve_ciudades("Madrid", "Barcelona", "Bilbao", "Valencia")
print(next(ciudades_devueltas))
print(next(ciudades_devueltas))