from io import open

"""
archivo_texto = open("archivo.txt", "w")# la opcion "w" es para escribir

frase = "Estupendo dia para estudiar Python \n el miercoles"
archivo_texto.write(frase)
archivo_texto.close()
"""
archivo_texto = open("archivo.txt", "r")# la opcion "r" es para leer
texto = archivo_texto.read()
archivo_texto.close()
print(texto)

#archivo_texto = open("archivo.txt", "r")
#lineas_texto = archivo_texto.readlines()
#archivo_texto.close()
#print(lineas_texto[0])

#archivo_texto = open("archivo.txt", "a")# la opcion "a" es para adicionar sin borrar lo existente
#archivo_texto.write("\n siempre es una buena ocacion para estudiar Python")
#archivo_texto.close()

#archivo_texto = open("archivo.txt", "r")
#archivo_texto.seek(11) #Pone el puntero en la posicion 11 para comenzar a leer desde esa posicion
#print(archivo_texto.read(11)) #Lee el archivo desde 0 hasta la posicion 11
#archivo_texto.seek(len(archivo_texto.read())/2) #Comienza a leer desde la mitad hasta el final
#archivo_texto.seek(len(archivo_texto.readline())) #Pone el cursos al final de la primera linea
#print(archivo_texto.read())
#archivo_texto.close()


archivo_texto = open("archivo.txt", "r+") #Abre el archivo en modo lectura y escritura, sobre escribe lo q haya desde la posicion del cursor en adelante 
lista_texto = archivo_texto.readlines()
lista_texto[1] = " Esta linea ha sido modificada desde el exterior \n"
archivo_texto.seek(0)
archivo_texto.writelines(lista_texto)
archivo_texto.close()
