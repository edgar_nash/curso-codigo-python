def funcion_decoradora(funcion_parametro):
	def funcion_interior(*args, **kwargs):
		print("Se relaizara una operacion")
		funcion_parametro(*args, **kwargs)
			# Acciones adicionales q decorar
		print("Operacion finalizada con exito")
	return funcion_interior

@funcion_decoradora
def suma(num1, num2, num3):
	print(num1+num2+num3)

@funcion_decoradora
def resta(num1, num2):
	print(num1-num2)

@funcion_decoradora
def potencia(base, exponente):
	print(pow(base,exponente))

suma(1, 5, 7)
resta(9, 5)
potencia(3, 2)