def test_listas ():
    '''
    >>>numeros = [1,2,3,4,5,6,7,8,9,0]
    for num in numeros:
	print(numeros)
    Resultado	
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

    >>>numeros = [1,2,3,4,5,6,7,8,9,0]
       i = 0
    for num in numeros:
	print(numeros[i])
	i = i + 1
    Resultado	
    1
    2
    3
    4
    5
    6
    7
    8
    9
    0
    '''

def llenar_lista ():
    '''(str) -> (list)
    Como llenar una lista
    '''
    paises = []
    pregunta = "Pais que te gustaria visitar: "
    pais = input(pregunta)
    while pais != '':
        paises.append(pais)
        pais = input(pregunta)
    return paises

def buscar_pais ():
    '''(str) -> (str)
    Encuentra un pais en una lista y no devuelve error si no se encuentra
    '''
    paises = ['Cuba', 'China', 'USA', 'Brasil']
    pais = input('Pais que quiere buscar: ')
    while pais != '':
            if not pais in paises:
                return "No existe el pais"
            else:
                return "Existe el pais"
                

def buscar_pais1 ():
    paises = ['Cuba', 'China', 'USA', 'Brasil']
    pais = input('Pais que quiere buscar: ')
    count = 0
    if pais in paises:
        paises.append(pais)
        count = paises.count(pais)
        paises.remove(pais)
        return 'Existe el pais en la lista '+str(count)+' vez y fue eliminado', paises
    else:
        return 'No existe el pais en la lista'

def encuentra_pais ():
    paises = ['Cuba', 'China', 'USA', 'Brasil']
    pais = input('Pais que quiere buscar: ')
    paises.extend(['Mexico'])
    paises.extend(['Japon', 'Chile', 'Colombia'])
    paises.insert(1, 'Francia')
    if paises.count(pais) > 0:
        print(paises)
        return 'Existe el pais en la lista'
    else:
         return 'No existe el pais en la lista'
