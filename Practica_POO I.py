class Coche():
	largoChasis = 250
	anchoChasis = 120
	ruedas = 4
	enMarcha = False

	def arrancar(self):
		self.enMarcha = True

	def estado(self):
		if self.enMarcha:
			return "El coche esta en marcha"
		else:
			return "El cohce esta parado"

miCoche = Coche() # Instanciar una clase o crear un objeto tipo Coche()

print("El largo del coche es: ", miCoche.largoChasis)
print("El coche tiene: ", miCoche.ruedas)
miCoche.arrancar()
print(miCoche.estado())