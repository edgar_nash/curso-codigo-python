def estado_agua (temperatura):
    '''(number) -> string
    Entra la temperatura del agua y te dice en que estado esta
    Ejs:
    >>> estado_agua()
    Entrar temperatura del agua-2
    Estado del agua SOLIDO
    '''
    if (type(temperatura) == int or type(temperatura) == float):
        if temperatura <= 0:
            print ("Estado del agua SOLIDO")
        elif temperatura > 0 and temperatura <= 99:
            print ("Estado del agua LIQUIDO")
        elif temperatura >= 100:
            print ("Estado del agua GASEOSO")
    else:
        print ("Introduzca una temperatura válida")
        
