import hashlib
#Imprimir metodos de cifrado soportados por la libreria
print(hashlib.algorithms_guaranteed)
cadena = "www.recursospython.com - Recursos Python"
cadena_cifrada = hashlib.sha256(cadena.encode(()))
#cadena_cifrada = hashlib.sha256(b"www.recursospython.com - Recursos Python")
print(cadena_cifrada)

import hashlib  # Se importa hashlib
contraseña = input("Contraseña\n") # El usuario ingresa la contraseña que va a utilizar
contraseña_cifrada = hashlib.sha512(contraseña.encode()) # Se codifica la contraseña y luego se 
                                                         # aplica el algoritmo de cifrado
print("Su contraseña cifrada es:")
print(contraseña_cifrada.hexdigest())  # Se imprime la contraseña cifrada en caracteres hexadecimales