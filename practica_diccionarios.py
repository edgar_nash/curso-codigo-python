midiccionario = {"Alemania":"Berlin", "Francia":"Paris", "Reino Unido":"Londres", "España":"Madrid"}
midiccionario["Italia"] = "Lisboa" #Agregar nuevo elemento
print(midiccionario)
midiccionario['Italia'] = 'Roma' # Modificar un elemento
print(midiccionario)
del midiccionario['Reino Unido'] #Eliminar un elemento
print(midiccionario)
print(midiccionario["Alemania"])