def recuperar_pares():
    '''(str) -> (str, number)
    De una cadena de caracteres recuperar los posiciones pares y los caracteres
    que contienen los guarda en una lista y los imprime
    Ejs:
    '''
    cadena = input("Entrar la cadena: ")
    list_caracteres = []
    for i in range(0, len(cadena), 2):
        list_caracteres.append(cadena[i]+"-"+str(i))
        print (cadena[i], i)
    return list_caracteres
    
