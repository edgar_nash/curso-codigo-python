class Coche():
	def desplazamiento(self):
		print("Me desplazo utilizando cuatro ruedas")

class Moto():
	def desplazamiento(self):
		print("Me desplazo utilizando dos ruedas")

class Camion():
	def desplazamiento(self):
		print("Me desplazo utilizando seis ruedas")

def desplazamientoVehiculo(vehiculo): 
	vehiculo.desplazamiento()
#El objeto vehiculo se comporta segun el tipo que sea Ej. Coche, Moto o Camion

miVehiculo = Coche()
#miVehiculo = Moto()
#miVehiculo = Camion()
desplazamientoVehiculo(miVehiculo)




