class Persona():
	def  __init__(self, nombre, edad, lugar_recidencia):
		self.nombre = nombre
		self.edad = edad
		self.lugar_recidencia = lugar_recidencia

	def descripcion(self):
		print("Nombre: ", self.nombre, "Edad: ", self.edad, "Residencia: ", self.lugar_recidencia)

class Empleado(Persona):
	def __init__(self, salario, antiguedad, nombre_empleado, edad_empleado, residencia_empleado):
		super().__init__(nombre_empleado, edad_empleado, residencia_empleado)
		self.salario = salario
		self.antiguedad = antiguedad
	def descripcion(self):
		super().descripcion()
		print(" Salario: ", self.salario, " Antiguedad: ", self.antiguedad)

Manuel = Persona("Manuel", 45, "Cuba")
#Manuel = Empleado(1500, 15, "Manuel", 55, "España")
Manuel.descripcion()
print(isinstance(Manuel, Empleado))

