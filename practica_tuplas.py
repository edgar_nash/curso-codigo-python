mitupla = ('Juan', 13, 1, 1995) # las tuplas pueden declararse sin () pero no es recomendable
nombre, dia, mes, agno = mitupla # desempaquetado de tupla asigna los valores de la tupla a las variables en el orden en q aparecen en la tupla
milista = list(mitupla) # convierte una tupla en lista
mitupla1 = tuple(milista) # convierte una lista en tupla
print('Juan' in mitupla)
print(mitupla.count(13))
print(len(mitupla))
print(nombre)
print(dia)
print(mes)
print(agno)
