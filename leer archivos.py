la funciona open tiene 4 opciones ('r' leer, 'w' escibir, 'a' añadir, 'r+' leer y escribir)
>>> briedma_archivo = 'C:/Users/Edgar/Desktop/biedma.txt'
>>> briedma = open(briedma_archivo, 'r')
>>> linea = briedma.readline()
'''Mientras linea no este vacio imprime linea por linea del archivo'''
>>> while linea !='':
	print(linea)
	linea = briedma.readline()
>>>briedma.close()

'''Mientras linea este dentro del archivo imprime la linea y elimina 
los saltos de linea '\n''' 
>>>for linea in briedma:
	print(linea, end = '')

'''Lee todo el contenido del archivo y lo devuelve'''
>>>print(briedma.read())

'''Lee todo el documento e indica donde estan los saltos de linea '\n'''
>>>briedma.readlines()

'''Al usar readlines() se guardan las lineas en una lista y podemos acceder
a cada auna de sus posiciones, es una ventaja con respeto a las otras respuestas'''
>>>lineas = briedma.readlines()
>>>for linea in lineas:
	   print(linea, end ='')