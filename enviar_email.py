import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
 
print('Preparando el contenido del correo electrónico...')
# Establecemos los datos del mensaje
contenidoMensaje = '''Hola, te envió este email desde app Python.
Por https://proyectoa.com'''
emailEmisor = 'edgar.nash@gmail.com'
usuarioEmail = 'edgar.nash@gmail.com'
contrasenaEmail = 'jejvffoaikscxeuc'
emailDestinatario = 'edgarnash@vivaldi.net'
asunto = 'Envío de mail desde Python'
servidorMail = 'smtp.gmail.com'
puertoServidorMail = 587
 
# Creamos el mensaje
mensaje = MIMEMultipart()
mensaje["From"] = emailEmisor
mensaje["To"] = emailDestinatario
mensaje["subject"] = asunto
mensaje.attach(MIMEText(contenidoMensaje, 'plain'))
 
print('Conectando con el servidor de correo electrónico...')
# Creamos la sesión SMTP para el envío del mail
servidorMail = smtplib.SMTP(servidorMail, puertoServidorMail)
servidorMail.starttls() # Habilitamos la seguridad (casi todos los servidores requieren de TLS)
servidorMail.login(usuarioEmail, contrasenaEmail)
print('Enviando el mensaje de correo electrónico...')
mensajeTexto = mensaje.as_string()
servidorMail.sendmail(emailEmisor, emailDestinatario, mensajeTexto)
servidorMail.quit()
print('El correo electrónico ha sido enviado desde Python [OK]')