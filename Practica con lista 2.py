lista = [1, 2, 2, 3, 5, 4, 5, 3, 2, 2, 1]

new_lista = sorted(lista) # sorted(set(lista)) ordena eliminando los repetidos
print(new_lista)

print(lista)

back_lista = list(reversed(lista))
print(back_lista)

# Ver si una cadena es palindome
# Itera sobre dos o más secuencias al mismo tiempo
palindrome = False
for l, b in zip(lista, back_lista):
    if l == b:
        palindrome = True
    else:
        palindrome = False
        break

if palindrome:
    print("Cadena palindrome")
else:
    print("La cadena no es palindrome")
