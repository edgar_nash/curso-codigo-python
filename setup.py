from setuptools import setup

setup (
	name = "paquetecalculos",
	version = "1.0",
	description = "Paquete de redondeo y potencia",
	author = "Nash",
	author_email = "edgar.nash@gmail.com",
	url = "www.facebook.com/edgar.p.nash",
	packages = ["calculos", "calculos.redondeo_potencia"]
	)