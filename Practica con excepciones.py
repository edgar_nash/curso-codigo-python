def dividir(x, y):
    try:
        resultado = x / y
    except ZeroDivisionError:
        print("!division por cero¡")
    else:
        print("el resultado es:", resultado)
    finally:
        print("ejecutando la clausula finally")

dividir(2, 1)
dividir(2, 0)
dividir('2', '1')