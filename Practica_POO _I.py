class Coche():
	def __init__(self):
		self.largoChasis = 250
		self.anchoChasis = 120
		self.__ruedas = 4 #Encapsular con "__" antes el nombre de la variable
		self.enMarcha = False

	def arrancar(self, arrancamos):
		self.enMarcha = arrancamos
		if (self.enMarcha):
			return "El coche esta en marcha"
		else:
			return "El coche esta parado"
		
	def estado(self):
		print("El coche tiene ", self.__ruedas, " ruedas. Un ancho ",
			self.anchoChasis, " y un largo de ", self.largoChasis)

miCoche = Coche() # Instanciar una clase o crear un objeto tipo Coche()
print(miCoche.arrancar(True))
miCoche.estado()

print("---------------Creamos segundo coche--------------------")
	
miCoche2 = Coche()
print(miCoche2.arrancar(False))
miCoche2.estado()