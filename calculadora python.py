def calculadora ():
    '''(number, number,...) -> (float)
    Entrada una operacion y varios numeros hace los calculos como una
    calculadora
    Ejs:
    '''
    num1 = 0
    num2 = 0
    operacion = 'SsRrMmDd'
    resultado = 0
    print('Esto es una calculadora, para sumar \'S\', para restar \'R\', para multiplicar \'M\', para dividir \'D\' y para saber el resultado \'=\'')
    operador = input('Entre la operacion: ')
    if operador in operacion:
        num1 = float(input('Entre el 1er numero: '))
        num2 = float(input('Entre el 2do numero: '))
        if operador == 'S' or operador == 's':
            resultado = num1 + num2
        elif operador == 'R' or operador == 'r':
            resultado = num1 - num2
        elif operador == 'M' or operador == 'm':
            if num1 == 0 or num2 == 0:
                print('Multiplicacion por cero es cero')
            else:
                resultado = num1 * num2
        else:
            if num1 == 0 or num2 == 0:
                print('Division por cero no permitida')
            else:
                resultado = num1 / num2
    else:
        print('Debe seleccionar una operacion')
    return 'El resultado de la operacion es: ' + str(resultado)

def calcul ():
    '''(number, number,...) -> (float)
    Entrar numeros mas las operaciones para realizar el calculo hasta que se entre el signo '='
    '''
    numeros = []
    operacion = 'SsRrMmDd'
    resultado = 0
    i = 0
    operador = ''
    num = float(input('Entre el numero: '))
    numeros.append(num)
    while operador != '=':
        operador = input('Entre la operacion: ')
        if operador in operacion:
            num = float(input('Entre el numero: '))
            numeros.append(num)
            if operador == 'S' or operador == 's':
                if resultado == 0:
                    resultado = numeros[i] + numeros[i + 1]
                    i = i + 1
                else:
                    resultado = resultado + numeros[i + 1]
                    i = i + 1
            elif operador == 'R' or operador == 'r':
                if resultado == 0:
                    resultado = numeros[i] - numeros[i + 1]
                    i = i + 1
                else:
                    resultado = resultado - numeros[i + 1]
                    i = i + 1
            elif operador == 'M' or operador == 'm':
                if numeros[i] == 0 or numeros[i + 1] == 0:
                    return 'La multiplicacion por 0 es 0'
                elif resultado == 0:
                    resultado = numeros[i] * numeros[i + 1]
                    i = i + 1
                else:
                    resultado = resultado * numeros[i + 1]
                    i = i + 1
            else:
                if numeros[i + 1] == 0:
                    return 'la division entre 0 no es permitida'
                elif resultado == 0:
                    resultado = numeros[i] / numeros[i + 1]
                    i = i + 1
                else:
                    resultado = resultado / numeros[i + 1]
                    i = i + 1
        else:
            if operador == '=':
                return 'El resultado de la operacion es: ' + str(resultado)
            else:
                print('Debe seleccionar una operacion')
    


        
        
