# con la tuberia | hace una union de los elementos de cada dic y si hay elementos repetidos
# solo escibre un elemento en la posicion donde 1ro aparezca el elemento repetido
d1 = {'hola':1, 'mundo':2}
d2 = {'gracias':3, 'python':4}
new_d1 = {**d1, **d2}
new_d2 = dict(d1, **d2)

print('primer dict', new_d1)
print('segundo dict', new_d2)


a = {1, 2, 3}
b = {3, 4, 5}

print( a | b )

a |= b # es lo mismo q a = a | b
print(a)