>>> s = "videotutoriales.com"
>>> s[6]
'u'
>>> s[5:14] + "en" + s[0:5]
'tutorialeenvideo'
>>> s[5:14] + " en " + s[0:5]
'tutoriale en video'
>>> s[5:15] + "en" + s[0:5]
'tutorialesenvideo'
>>> s[5:15] + " en " + s[0:5]
'tutoriales en video'
>>> s = s[5:15] + " en " + s[0:5]
>>> s
'tutoriales en video'
>>> s = s[-6:-1] + "s" + s[0:9] + ".com"
>>> s
' videstutoriale.com'
>>> s = s[5:15] + " en " + s[0:5]
>>> s
'stutoriale en  vide'
>>> s = s[5:15] + " en " + s[0:5]
>>> s
'riale en   en stuto'
>>> s = "tutoriales en video"
>>> s = s[13:]+"s"+[:10]+".com"
SyntaxError: invalid syntax
>>> s = s[13:]+"s"+s[:10]+".com"
>>> s
' videostutoriales.com'
>>> s = s[14:]+"s"+s[:10]+".com"
>>> s
'les.coms videostut.com'
>>> s = "tutoriales en video"
>>> s = s[14:]+"s"+s[:10]+".com"
>>> s
'videostutoriales.com'
>>> 