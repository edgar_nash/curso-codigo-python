import pickle

class Persona:
	def __init__(self, nombre, genero, edad):
		self.nombre = nombre
		self.genero = genero
		self.edad = edad
		print("Se ha creado una persona nueva con el nombre de ", self.nombre)

	def __str__(self):
		return "{} {} {}".format(self.nombre, self.genero, self.edad)

class ListaPersonas:
	personas = []
	
	def __init__(self):
		
		with open("ficheroExterno", "ab+") as listaDePersonas:
			listaDePersonas.seek(0)
		
			try:
				self.personas = pickle.load(listaDePersonas)
				print("Se cargaron {} personas del fichero externo".format(len(self.personas)))
			except:
				print("El fichero esta vacio")
		
	def agregarPersonas(self, p):
		self.personas.append(p)
		self.guardarPersonasEnFicheroExterno()

	def mostrarPersonas(self):
 		for p in self.personas:
 			print(p)

	def guardarPersonasEnFicheroExterno(self):
 		with open("ficheroExterno", "wb") as listaDePersonas:
 			pickle.dump(self.personas, listaDePersonas)

	def mostrarInfoFicheroExterno(self):
 		print("La informacion del fichero externo es la siguiente:")
 		for p in self.personas:
 			print(p)


miLista = ListaPersonas()
persona = Persona("Ana", "Femenino", 19)
miLista.agregarPersonas(persona)
miLista.mostrarInfoFicheroExterno()
print(len(miLista.personas))