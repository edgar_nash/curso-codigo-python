nombreUsuario = input("Introduce tu nombre de usuario: ")
#print("El nombre es : ", nombreUsuario.upper()) #transforma el text a mayuscula
#print("El nombre es : ", nombreUsuario.lower()) #transforma el text a minuscula
print("El nombre es : ", nombreUsuario.capitalize()) #transforma la 1ra letra en mayuscula

edad = input("Introduce una edad: ")

while (edad.isdigit() == False):
	print("Por favor, introduce un valor numerico")
	edad = input("Introduce una edad: ")

if (int(edad) < 18):
	print("No puede pasar")
else:
	print("Puede pasar")