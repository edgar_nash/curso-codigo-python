def area_rectangulo (largo, ancho):
    '''(number, number) -> number
    Devuelve el areade un rectangulo al pasarle su alto y ancho
    Ejemplos
    area_rectangulo(7, 8)
    >>>56
    area_rectangulo(3.5, 5.8)
    >>>20.3
    '''
	
    area = largo * ancho
    return area

def perimetro_rectangulo (largo, ancho):
    perimetro = (largo + ancho) * 2
    return perimetro

